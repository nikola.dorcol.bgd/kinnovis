<?php

namespace Database\Factories;

use App\Models\FruitType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class FruitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'type_id' => FruitType::inRandomOrder()->first()->id,
            'weight' => fake()->numberBetween(1, 100),
        ];
    }
}
