<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Fruit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $fruits = [
            'Apple',
            'Apricot',
            'Avocado',
            'Banana',
            'Blackberry',
            'Blueberry',
            'Cantaloupe',
            'Cherry',
            'Coconut',
            'Cranberry',
            'Date',
            'Dragonfruit',
            'Durian',
            'Elderberry',
            'Fig',
            'Grape',
            'Grapefruit',
            'Guava',
            'Honeydew',
            'Jackfruit',
            'Kiwi',
            'Kumquat',
            'Lemon',
            'Lime',
            'Lychee',
            'Mango',
            'Melon',
            'Mulberry',
            'Nectarine',
            'Orange',
            'Papaya',
            'Peach',
            'Pear',
            'Pineapple',
            'Plum',
            'Pomegranate',
            'Raspberry',
            'Redcurrant',
            'Strawberry',
            'Tangerine',
            'Watermelon',
            'Yellow watermelon',
            'Starfruit',
            'Passionfruit',
            'Persimmon',
            'Quince',
            'Ugli fruit',
            'Cherimoya',
        ];

        DB::table('fruit_types')->insert([
            ['name' => 'Tropical Fruit'],
            ['name' => 'Citrus Fruit'],
            ['name' => 'Berries'],
            ['name' => 'Melon'],
        ]);

        foreach ($fruits as $fruit) {
            Fruit::factory()->create([
                'name' => $fruit
            ]);
        }
    }
}
