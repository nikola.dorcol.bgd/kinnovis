<?php

use App\Http\Controllers\Api\ConfigController;
use App\Http\Controllers\Api\FruitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'config'], function() {
    Route::get('/', [ConfigController::class, 'config']);
});

Route::group(['prefix' => 'fruits'], function() {
    // This could be resource group of routes in real case
    Route::get('/', [FruitController::class, 'index']) ;
});
