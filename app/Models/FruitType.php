<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FruitType extends Model
{
    use HasFactory;

    protected $fillable = [];

    public function fruits(): HasMany
    {
        return $this->hasMany(Fruit::class, 'type_id', 'id');
    }
}
