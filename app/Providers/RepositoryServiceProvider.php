<?php

namespace App\Providers;

use App\Repository\Eloquent\FruitRepository;
use App\Repository\Eloquent\FruitTypeRepository;
use App\Repository\FruitRepositoryInterface;
use App\Repository\FruitTypeRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(FruitRepositoryInterface::class, FruitRepository::class);
        $this->app->bind(FruitTypeRepositoryInterface::class, FruitTypeRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
