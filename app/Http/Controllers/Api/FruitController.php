<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FruitResource;
use App\Repository\FruitRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FruitController extends Controller
{
    /**
     * @param Request $request
     * @param FruitRepositoryInterface $fruitRepository
     * @return AnonymousResourceCollection
     */
    public function index(Request $request, FruitRepositoryInterface $fruitRepository): AnonymousResourceCollection
    {
        return FruitResource::collection($fruitRepository->getAllFruitsPaginated($request->all()));
    }
}
