<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FruitTypeResource;
use App\Models\Fruit;
use App\Repository\FruitTypeRepositoryInterface;
use Illuminate\Http\JsonResponse;

class ConfigController extends Controller
{
    /**
     * @param FruitTypeRepositoryInterface $fruitTypeRepository
     * @return JsonResponse
     */
    public function config(FruitTypeRepositoryInterface $fruitTypeRepository): JsonResponse
    {
        return response()->json([
            'fields' => [
                'fruit_types' => [
                    'key' => 'type',
                    'label' => 'Fruit Type',
                    'type' => 'select',
                    'options' => FruitTypeResource::collection($fruitTypeRepository->getAllTypes()),
                ],
                'search' => [
                    'key' => 'search',
                    'label' => 'Search',
                    'type' => 'text'
                ],
            ],
        ]);
    }
}
