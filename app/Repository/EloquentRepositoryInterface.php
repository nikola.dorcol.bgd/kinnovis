<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;

interface EloquentRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param int $perPage
     * @return mixed
     */
    public function paginate(int $perPage = 15);
}
