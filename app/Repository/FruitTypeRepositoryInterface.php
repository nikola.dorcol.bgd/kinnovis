<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;

interface FruitTypeRepositoryInterface
{
    /**
     * Get all fruit types from db
     *
     * @return Collection
     */
    public function getAllTypes(): Collection;
}
