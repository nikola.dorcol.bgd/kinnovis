<?php

namespace App\Repository;

use Illuminate\Pagination\LengthAwarePaginator;

interface FruitRepositoryInterface
{
    /**
     * Get all fruits from DB
     * @return LengthAwarePaginator
     */
    public function getAllFruitsPaginated(array $params): LengthAwarePaginator;
}
