<?php

namespace App\Repository\Eloquent;

use App\Models\Fruit;
use App\Repository\FruitRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class FruitRepository extends BaseRepository implements FruitRepositoryInterface
{
    /**
     * @var string
     */
    protected $model = Fruit::class;

    /**
     * @return LengthAwarePaginator
     */
    public function getAllFruitsPaginated(array $params): LengthAwarePaginator
    {
        $this->model = $this->model->query();

        if (isset($params['type'])) {
            $this->model->whereHas('type', function ($q) use ($params) {
                $q->where('id', $params['type']);
            });
        }

        if (isset($params['search'])) {
            $searchText = $params['search'];
            $fillables = (new Fruit())->getFillable();

            $this->model->where(function ($query) use ($searchText, $fillables) {
                foreach ($fillables as $field) {
                    $query->orWhere($field, 'like', '%'.$searchText.'%');
                }
            });
        }

        if (isset($params['key']) && isset($params['direction'])) {
            $this->model->orderBy($params['key'], $params['direction']);
        }

        return $this->paginate();
    }
}
