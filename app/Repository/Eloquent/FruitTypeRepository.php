<?php

namespace App\Repository\Eloquent;

use App\Models\FruitType;
use App\Repository\FruitTypeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FruitTypeRepository extends BaseRepository implements FruitTypeRepositoryInterface
{
    /**
     * @var string
     */
    protected $model = FruitType::class;

    /**
     * @return Collection
     */
    public function getAllTypes(): Collection
    {
        return $this->all();
    }
}
