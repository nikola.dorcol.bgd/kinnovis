<?php

namespace App\Repository\Eloquent;

use App\Repository\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {
        $this->model = $this->getModel();
    }

    /**
     * Create model instance
     *
     * @return Model
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function getModel(): Model
    {
        return app()->make($this->model);
    }

    public function all(): Collection
    {
        return $this->model->get();
    }

    public function paginate(int $perPage = 15)
    {
        return $this->model->paginate($perPage);
    }
}
