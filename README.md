# KINNOVIS Project

This project is built using Laravel Sail, a lightweight command-line interface for Laravel development environment based on Docker.

## Setup

1. Clone the repository
    


2. Navigate into the project directory:

    cd your_project


3. Copy the `.env.example` file to `.env` for both Laravel and Vue applications:

    cp client/.env.example client/.env
    cp .env.example .env


4. Run the Sail command to start the development environment:

    ./vendor/bin/sail up



5. In a separate terminal window, run the following command to install Laravel dependencies:

    ./vendor/bin/sail composer install

## Usage

The Laravel application is accessible at `http://localhost` and the Vue application is accessible at `http://localhost:CLIEN_PORT`.

## API Contract

The API contract and description can be found in the `docs/openapi.yaml` file.

## Troubleshooting

- If you encounter any issues while running the Sail command, try running it with `sudo`.
- If you encounter any issues with the Laravel application, check the logs in the `storage/logs` directory.
