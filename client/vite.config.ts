import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

require('dotenv').config()

// https://vitejs.dev/config/
export default defineConfig({
    server: {
        port: process.env.CLIENT_PORT,
    },
    plugins: [vue()],
    resolve: {
        alias: {
            "@": fileURLToPath(new URL("./src", import.meta.url)),
        },
    },
});
