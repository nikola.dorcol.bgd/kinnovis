import axios from "axios";

const baseURL = "http://localhost/api/"; // Replace this with your API base URL

export default {
    install: (app: any) => {
        app.config.globalProperties.$axios = axios.create({
            baseURL
        });
    },
};
